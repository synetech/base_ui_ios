//
//  SYNProgressBar.swift
//  Cemex SalesRep
//
//  Created by Daniel Rutkovsky on 6/20/17.
//  Copyright © 2017 CEMEX. All rights reserved.
//

import UIKit

public protocol SYNProgressBarProtocol {
    func setupProgressBar(backgroundImage: UIImage?, initialProgress: Double)
    func setProgress(progress: Double, animate: Bool)
}
