//
//  UIAlerAction.swift
//
//  Created by Filip Bulander on 24/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import UIKit

public extension UIAlertAction {
    func setTitleColor(color: UIColor) {
        self.setValue(color, forKey: "titleTextColor")
    }
}
