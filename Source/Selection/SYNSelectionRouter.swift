//
//  SelectionRouter.swift
//  Expensa
//
//  Created by Dominik on 28/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import Foundation
import RxSwift
import SYNBase

public enum Background {
    case image(UIImage)
    case color(color: UIColor)
}

public protocol SYNSelectionRouterProtocol {
    func didSelectItem(item: SYNSelectionItem)
    func didCancelRouting()
}

open class SYNSelectionRouter {
    fileprivate var didSelect: ((SYNSelectionItem) -> Void)?
    fileprivate var didCancel: (() -> Void)?
    fileprivate var navigationController: UINavigationController?

    fileprivate var background: Background?
    fileprivate var selectionColor: UIColor?
    fileprivate var cellFont: UIFont?
    fileprivate var customCell: SYNSelectionTableViewCell.Type?

    private var viewController: UIViewController?

    public func start(navigationController: UINavigationController?,
                      didSelect: @escaping (SYNSelectionItem) -> Void,
                      didCancel: @escaping () -> Void) {
        self.didCancel = didCancel
        self.didSelect = didSelect
        self.navigationController = navigationController
        showSelectionView()
    }

    public convenience init(title: String,
                            array: [SYNSelectionItem],
                            background: Background? = Background.color(color: .white),
                            selectionColor: UIColor? = .gray,
                            cellFont: UIFont? = UIFont.systemFont(ofSize: 15),
                            customCell: SYNSelectionTableViewCell.Type? = SYNSelectionTableViewCell.self) {
        let observable = Observable<[SYNSelectionItem]>.create { (observable) -> Disposable in
            observable.onNext(array)
            return Disposables.create()
        }
        self.init()
        self.viewController = nil
        self.viewController = SYNSelectionBuilder.buildViewController(router: self,
                                                                      title: title,
                                                                      items: observable,
                                                                      background: background!,
                                                                      selectionColor: selectionColor!,
                                                                      cellFont: cellFont!,
                                                                      customCell: customCell!)
    }

    private func showSelectionView() {
        if let viewController = viewController {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension SYNSelectionRouter: SYNSelectionRouterProtocol {
    public func didCancelRouting() {
        self.navigationController?.popViewController(animated: true)
        self.didCancel?()
    }

    public func didSelectItem(item: SYNSelectionItem) {
        self.didSelect?(item)
        self.navigationController?.popViewController(animated: true)
    }
}
