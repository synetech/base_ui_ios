//
//  SelectionItem.swift
//  AppStore
//
//  Created by Dominik on 28/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import UIKit

/// Protocol of an item for selection ViewController
public protocol SYNSelectionItem {
    var id: String { get }
    var title: String { get }
    var image: UIImage? { get }
}
