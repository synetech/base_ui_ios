//
//  SelectionTableViewCell.swift
//  Expensa
//
//  Created by Filip Bulander on 29/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import Stevia
import UIKit

/// Base Cell class to be overriden or changed while selector implementation
open class SYNSelectionTableViewCell: UITableViewCell {

    private let stackView = { () -> UIStackView in
        let stack = UIStackView()
        stack.alignment = .center
        stack.distribution = .fill
        stack.spacing = 8
        stack.axis = .horizontal
        return stack
    } ()

    private let ivSelection = UIImageView()

    private let lblSelection = { () -> UILabel in
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.lineBreakMode = .byTruncatingMiddle
        return label
    } ()

    private let view = UIView()

    /// Base Cell constructor to be used
    ///
    /// - Parameters:
    ///   - style: Cell UITableViewCellStyle to be used
    ///   - reuseIdentifier: Cell String for reuse
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setTitle(title: String) {
        lblSelection.text = title
    }

    func setImage(image: UIImage?) {
        ivSelection.image = image
        ivSelection.isHidden = image == nil
    }

    /// Method which defines layout of a used cell
    open func setupCell() {
        sv(stackView)
        layout(14,
            |-16 - stackView - 16-|,
        14)
        stackView.addArrangedSubview(ivSelection, lblSelection)
        view.backgroundColor = UIColor.white
        selectedBackgroundView = view
        ivSelection.contentMode = .scaleAspectFit
        ivSelection.heightEqualsWidth()
    }
}

extension SYNSelectionTableViewCell {
    /// Returns identifiers of a cell
    ///
    /// - Returns: identifiers of a cell
    public static func getIdentifier() -> String {
        return "SelectionTableVieCell"
    }
}
