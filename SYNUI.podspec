#
#  Be sure to run `pod spec lint SYNBaseProject.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "SYNUI"
  s.version      = "2.0.2"
  s.summary      = "A base UI project that probivies a unfied UI elementets that could be used in all our apps"
  s.description  = <<-DESC
                    A base UI project that probivies a unfied UI elementets that could be used in all our apps.
                    This repo provides a basic UI componets that could be reused.
                    DESC
  s.homepage     = "https://bitbucket.org/synetech/base_ui_ios"
  s.license      = "MIT"
  s.author       = { "Daniel Rutkovsky" => "daniel.rutkovsky@synetech.cz" }
  
  s.source       = { :git => "git@bitbucket.org:synetech/base_ui_ios.git", :tag => "#{s.version}" }
  # s.source_files  = "Source",
  
  s.requires_arc = true
  s.platform     = :ios, "10.0"
  s.dependency 'SteviaLayout', '~> 4'

  s.subspec 'PageViewController' do |ss|
    ss.source_files = 'Source/UIPageViewController/**/*.swift'
  end

  s.subspec 'InputViews' do |ss|
    ss.source_files = 'Source/InputViews/**/*.swift'
  end

  s.subspec 'TabBar' do |ss|
    ss.source_files = 'Source/TabBar/**/*.swift'
    ss.dependency 'SYNBase/VIPER'
  end

  s.subspec 'NavigationBar' do |ss|
    ss.source_files = 'Source/NavigationBar/**/*.swift'
  end

  s.subspec 'Extensions' do |ss|
    ss.source_files = 'Source/Extensions/**/*.swift'
  end
  
  s.subspec 'Selection' do |ss|
    ss.source_files = 'Source/Selection/**/*.swift'
    ss.dependency 'RxSwift'
    ss.dependency 'SYNBase'
  end

  # s.subspec 'Routing' do |ss|
  #   ss.source_files = 'Source/Routing/**/*.swift'
  # end

  # s.subspec 'VIPER' do |ss|
  #   ss.source_files = 'Source/VIPER/**/*.swift'
  #   ss.exclude_files = 'Source/VIPER/SVProgressHUD'
  #   # ss.default_subspec = 'SVProgressHUD'

  #   ss.subspec 'SVProgressHUD' do |sss|
  #     sss.source_files = 'Source/VIPER/SVProgressHUD/**/*.swift'
  #     sss.dependency 'SVProgressHUD', '~> 2.1'
  #    end
  # 
  # end

end
